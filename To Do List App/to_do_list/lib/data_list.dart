class ToDoDataList {
  String? title;
  String? description;
  String? date;

  ToDoDataList(
      {required this.title, required this.description, required this.date});
}

List dataList = [
  ToDoDataList(
    title: "Lorem Ipsum is simply setting industry. ",
    description:
        "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    date: "February 28, 2024",
  ),
  ToDoDataList(
    title: "Lorem Ipsum is simply setting industry. ",
    description:
        "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    date: "February 28, 2024",
  ),
  ToDoDataList(
    title: "Lorem Ipsum is simply setting industry. ",
    description:
        "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    date: "February 28, 2024",
  ),
];
