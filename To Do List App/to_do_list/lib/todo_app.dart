import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// ignore: depend_on_referenced_packages
import 'package:intl/intl.dart';
import 'package:to_do_list/data_list.dart';
import 'package:to_do_list/colors.dart';

class ToDoList extends StatefulWidget {
  const ToDoList({super.key});

  @override
  State<ToDoList> createState() => _ToDoListState();
}

class _ToDoListState extends State<ToDoList> {
  bool isEdit = false;
  TextEditingController titleText = TextEditingController();
  TextEditingController descriptionText = TextEditingController();
  TextEditingController dateText = TextEditingController();

  void editTask(ToDoDataList? toDoDataListobj) {
    titleText.text = toDoDataListobj!.title!;
    descriptionText.text = toDoDataListobj.description!;
    dateText.text = toDoDataListobj.date!;

    showBottomSheet(true, toDoDataListobj);
  }

  void submit(bool isEdit, [ToDoDataList? toDoDataListobj]) {
    if (titleText.text.isNotEmpty &&
        descriptionText.text.isNotEmpty &&
        dateText.text.isNotEmpty) {
      if (!isEdit) {
        setState(() {
          dataList.add(
            ToDoDataList(
              title: titleText.text.trim(),
              description: descriptionText.text.trim(),
              date: dateText.text.trim(),
            ),
          );
        });
      } else {
        setState(() {
          toDoDataListobj!.title = titleText.text.trim();
          toDoDataListobj.description = descriptionText.text.trim();
          toDoDataListobj.date = dateText.text.trim();
        });
      }
      titleText.clear();
      descriptionText.clear();
      dateText.clear();
      Navigator.pop(context);
    }
  }

  void showBottomSheet(bool isEdit, [ToDoDataList? toDoDataListobj]) {
    showModalBottomSheet(
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(50),
          topRight: Radius.circular(50),
        ),
      ),
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsetsDirectional.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 30,
              ),
              SizedBox(
                height: 28,
                child: (!isEdit)
                    ? Text(
                        "Create Task",
                        style: GoogleFonts.quicksand(
                          color: const Color.fromRGBO(0, 0, 0, 1),
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                        ),
                      )
                    : Text(
                        "Edit Task",
                        style: GoogleFonts.quicksand(
                          color: const Color.fromRGBO(0, 0, 0, 1),
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const SizedBox(
                    width: 33,
                  ),
                  Text(
                    "Title",
                    style: GoogleFonts.quicksand(
                      color: const Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 2,
              ),
              SizedBox(
                height: 40,
                width: 330,
                child: TextField(
                  controller: titleText,
                  cursorColor: const Color.fromRGBO(0, 139, 148, 1),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    enabledBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(0, 139, 148, 1),
                      ),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(0, 139, 148, 1),
                        width: 2,
                      ),
                    ),
                    errorBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(0, 139, 148, 1),
                        width: 2,
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 25,
              ),
              Row(
                children: [
                  const SizedBox(
                    width: 33,
                  ),
                  Text(
                    "Description",
                    style: GoogleFonts.quicksand(
                      color: const Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 2,
              ),
              SizedBox(
                height: 72,
                width: 330,
                child: TextField(
                  controller: descriptionText,
                  cursorColor: const Color.fromRGBO(0, 139, 148, 1),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    enabledBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(0, 139, 148, 1),
                      ),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(0, 139, 148, 1),
                        width: 2,
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const SizedBox(
                    width: 33,
                  ),
                  Text(
                    "Date",
                    style: GoogleFonts.quicksand(
                      color: const Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 2,
              ),
              SizedBox(
                height: 40,
                width: 330,
                child: TextField(
                  controller: dateText,
                  readOnly: true,
                  cursorColor: const Color.fromRGBO(0, 139, 148, 1),
                  decoration: InputDecoration(
                    suffixIcon: GestureDetector(
                      onTap: () async {
                        DateTime? pickDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2022),
                          lastDate: DateTime(2025),
                        );
                        String dateFormat =
                            DateFormat.yMMMMd().format(pickDate!);

                        setState(() {
                          dateText.text = dateFormat;
                        });
                      },
                      child: const Icon(
                        Icons.calendar_month_outlined,
                        color: Color.fromRGBO(0, 0, 0, 0.7),
                      ),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                    enabledBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(0, 139, 148, 1),
                      ),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(0, 139, 148, 1),
                        width: 2,
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 35,
              ),
              SizedBox(
                height: 50,
                width: 300,
                child: ElevatedButton(
                  style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(
                      Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      if (!isEdit) {
                        submit(false);
                      } else {
                        submit(true, toDoDataListobj);
                      }
                    });
                  },
                  child: Text(
                    "Submit",
                    style: GoogleFonts.quicksand(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      fontSize: 20,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 35,
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
        title: Text(
          "To-do list",
          style: GoogleFonts.quicksand(
            color: const Color.fromRGBO(255, 255, 255, 1),
            fontWeight: FontWeight.w700,
            fontSize: 26,
          ),
        ),
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
            child: Container(
              height: 112,
              width: 330,
              decoration: BoxDecoration(
                color: colors[index % colors.length],
                borderRadius: BorderRadius.circular(10),
                boxShadow: const [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.1),
                    blurRadius: 20,
                    spreadRadius: 1,
                    offset: Offset(0, 10),
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: 52,
                              width: 52,
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color.fromRGBO(255, 255, 255, 1),
                                boxShadow: [
                                  BoxShadow(
                                    blurRadius: 10,
                                    color: Color.fromRGBO(0, 0, 0, 0.07),
                                  ),
                                ],
                              ),
                              child: Image.asset("images/image.jpg"),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            SizedBox(
                              height: 15,
                              width: 243,
                              child: Text(
                                dataList[index].title,
                                style: GoogleFonts.quicksand(
                                  color: const Color.fromRGBO(0, 0, 0, 1),
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            SizedBox(
                              height: 44,
                              width: 243,
                              child: Text(
                                dataList[index].description,
                                style: GoogleFonts.quicksand(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                  color: const Color.fromRGBO(84, 84, 84, 1),
                                ),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        const SizedBox(
                          width: 2,
                        ),
                        Text(
                          dataList[index].date,
                          style: GoogleFonts.quicksand(
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                            color: const Color.fromRGBO(132, 132, 132, 1),
                          ),
                        ),
                        const Spacer(),
                        GestureDetector(
                          onTap: () {
                            editTask(dataList[index]);
                          },
                          child: const Icon(
                            Icons.edit,
                            size: 18,
                            color: Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              dataList.removeAt(index);
                            });
                          },
                          child: const Icon(
                            Icons.delete_outline_outlined,
                            size: 18,
                            color: Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        },
        itemCount: dataList.length,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            titleText.clear();
            descriptionText.clear();
            dateText.clear();
            showBottomSheet(false);
          });
        },
        backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
        child: const Icon(
          Icons.add,
          size: 45,
          color: Color.fromRGBO(255, 255, 255, 1),
        ),
      ),
    );
  }
}
