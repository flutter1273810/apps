import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:to_do_app/to_do_modelclass.dart';

dynamic database;
List<Map<String, dynamic>> dbData = [];

void insertData(ToDoModel obj) async {
  Database loacalDB = await database;

  await loacalDB.insert(
    "ToDoData",
    obj.mapEntry(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

void getData(VoidCallback callback) async {
  Database localDb = await database;

  dbData = await localDb.query("ToDoData");
  print(dbData);
  callback();
}

void updateData(ToDoModel obj) async {
  Database localDb = await database;
  print(obj.taskId);
  await localDb.update(
    "ToDoData",
    obj.mapEntry(),
    where: "taskId=?",
    whereArgs: [obj.taskId],
    // conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

void deleteData(String title) async {
  Database localDb = await database;

  await localDb.delete(
    "ToDoData",
    where: "title=?",
    whereArgs: [title],
  );
}

void createDatabase(VoidCallback callback) async {
  database = await openDatabase(
    join(await getDatabasesPath(), "tododb.db"),
    version: 1,
    onCreate: (db, version) async {
      await db.execute(''' CREATE TABLE ToDoData(
          taskId INTEGER PRIMARY KEY,
          title TEXT,
          description TEXT,
          date TEXT
        )
        ''');
    },
  );
  print(await getDatabasesPath());
  getData(callback);
}
