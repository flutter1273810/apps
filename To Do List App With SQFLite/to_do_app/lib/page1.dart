import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:to_do_app/colors.dart';
import 'package:to_do_app/sqf_data.dart';
import 'package:to_do_app/to_do_modelclass.dart';

class Page1 extends StatefulWidget {
  const Page1({super.key});

  @override
  State createState() => _Page1State();
}

List<ToDoModel> myList = [
  ToDoModel(
    title: "Lorem Ipsum is simply setting industry.",
    description:
        "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    date: "10 July 2023",
  ),
];

class _Page1State extends State {
  callback() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();

    createDatabase(callback);
  }

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController dateController = TextEditingController();

  Color myColor(int index) {
    int temp = index % colorList.length;
    return colorList[temp];
  }

  bool flag = false;
  void myBottomSheet(BuildContext context, int? index) {
    if (flag == true) {
      titleController.text = dbData[index!]["title"];
      descriptionController.text = dbData[index]["description"];
      dateController.text = dbData[index]["date"];
    }
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.only(
            top: 20,
            right: 20,
            left: 20,
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Create Task",
                style: GoogleFonts.quicksand(
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Title",
                    style: GoogleFonts.quicksand(
                      fontSize: 11,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                    child: TextField(
                      controller: titleController,
                      cursorHeight: 15,
                      cursorColor: const Color.fromRGBO(0, 139, 148, 1),
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                  Text(
                    "Description",
                    style: GoogleFonts.quicksand(
                      fontSize: 11,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                  SizedBox(
                    height: 72,
                    child: TextField(
                      controller: descriptionController,
                      cursorColor: const Color.fromRGBO(0, 139, 148, 1),
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                  Text(
                    "Date",
                    style: GoogleFonts.quicksand(
                      fontSize: 11,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                    child: TextField(
                      controller: dateController,
                      readOnly: true,
                      cursorHeight: 15,
                      cursorColor: const Color.fromRGBO(0, 139, 148, 1),
                      decoration: InputDecoration(
                        suffixIcon: IconButton(
                                    onPressed: () async {
                                      DateTime? pickDate = await showDatePicker(
                                        context: context,
                                        initialDate: DateTime.now(),
                                        firstDate: DateTime(2022),
                                        lastDate: DateTime(2025),
                                      );
                                      String dateFormat =
                                          DateFormat.yMMMMd().format(pickDate!);

                                      dateController.text = dateFormat;
                                    },
                                    icon: const Icon(
                                      Icons.calendar_month_outlined,
                                      weight: 1,
                                      size: 20,
                                    ),
                                  ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                          borderSide: const BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                            width: 2.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                ],
              ),
              GestureDetector(
                onTap: () {
                  if (titleController.text.isNotEmpty &&
                      descriptionController.text.isNotEmpty &&
                      dateController.text.isNotEmpty) {
                    setState(() {
                      if (flag == false) {
                        // myList.add(
                        //   ToDoModel(
                        //     title: titleController.text.trim(),
                        //     description: descriptionController.text.trim(),
                        //     date: dateController.text.trim(),
                        //   ),
                        // );

                        insertData(
                          ToDoModel(
                            title: titleController.text.trim(),
                            description: descriptionController.text.trim(),
                            date: dateController.text.trim(),
                          ),
                        );
                        getData(callback);
                      } else {
                        updateData(
                          ToDoModel(
                            taskId: dbData[index!]["taskId"],
                            title: titleController.text.trim(),
                            description: descriptionController.text.trim(),
                            date: dateController.text.trim(),
                          ),
                          
                        );
                        getData(callback);
                        flag = false;
                      }
                      titleController.clear();
                      descriptionController.clear();
                      dateController.clear();
                      Navigator.pop(context);
                    });
                  }
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                  ),
                  child: Container(
                    alignment: Alignment.center,
                    height: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: const Color.fromRGBO(0, 139, 148, 1),
                    ),
                    child: Text(
                      "Submit",
                      style: GoogleFonts.inter(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                        color: const Color.fromRGBO(255, 255, 255, 1),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 25,
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
        title: Text(
          "To-do list",
          style: GoogleFonts.quicksand(
            textStyle: const TextStyle(
              fontSize: 26,
              fontWeight: FontWeight.w700,
              color: Color.fromRGBO(255, 255, 255, 1),
            ),
          ),
        ),
      ),
      body: ListView.builder(
        itemCount: dbData.length,
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.only(
            top: 30,
            left: 15,
            right: 15,
          ),
          child: Container(
            height: 112,
            width: double.infinity,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: myColor(index),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          alignment: Alignment.center,
                          height: 52,
                          width: 52,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            color: const Color.fromRGBO(255, 255, 255, 1),
                          ),
                          child: Image.asset("images/image1.png"),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            dbData[index]["title"],
                            style: GoogleFonts.quicksand(
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                              color: const Color.fromRGBO(0, 0, 0, 1),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            child: Text(
                              dbData[index]["description"],
                              style: GoogleFonts.quicksand(
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                                color: const Color.fromRGBO(84, 84, 84, 1),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      dbData[index]["date"],
                      style: GoogleFonts.quicksand(
                        fontSize: 10,
                        fontWeight: FontWeight.w500,
                        color: const Color.fromRGBO(132, 132, 132, 1),
                      ),
                    ),
                    const Spacer(),
                    GestureDetector(
                      onTap: () {
                        flag = true;
                        myBottomSheet(context, index);
                      },
                      child: const SizedBox(
                        child: Icon(
                          Icons.edit_outlined,
                          color: Color.fromRGBO(0, 139, 148, 1),
                          size: 18,
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          deleteData(dbData[index]["title"]);
                          getData(callback);
                        });
                      },
                      child: const SizedBox(
                        child: Icon(
                          Icons.delete_outline,
                          color: Color.fromRGBO(0, 139, 148, 1),
                          size: 18,
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        shape: const CircleBorder(),
        onPressed: () {
          myBottomSheet(context, null);
        },
        backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
        child: const Icon(Icons.add, color: Colors.white, size: 46),
      ),
    );
  }
}
