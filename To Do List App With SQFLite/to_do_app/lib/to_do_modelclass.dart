class ToDoModel {
  String title;
  String description;
  String date;
  int? taskId;
  ToDoModel({
    required this.title,
    required this.description,
    required this.date,
    this.taskId,
  });

  Map<String, dynamic> mapEntry() {
    return {
      "taskId":taskId,
      "title": title,
      "description": description,
      "date": date,
    };
  }
}
