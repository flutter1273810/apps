class QuizQuestions {
  final String? question;
  final List<String>? options;
  final int? answerIndex;

  const QuizQuestions(
      {required this.question,
      required this.options,
      required this.answerIndex});
}

List allQuestions = [
  const QuizQuestions(
    question: "1.Who is Founder Of MicroSoft?",
    options: [
      "1. Bill Gates",
      "2. Larry Page",
      "3. Steve Jobs",
      "4. Elone Musk",
    ],
    answerIndex: 0,
  ),
  const QuizQuestions(
    question: "2.Who is Founder Of Apple?",
    options: [
      "1. Bill Gates",
      "2. Larry Page",
      "3. Steve Jobs",
      "4. Elone Musk",
    ],
    answerIndex: 2,
  ),
  const QuizQuestions(
    question: "3.Who is Founder Of Tesla?",
    options: [
      "1. Bill Gates",
      "2. Larry Page",
      "3. Steve Jobs",
      "4. Elone Musk",
    ],
    answerIndex: 3,
  ),
  const QuizQuestions(
    question: "4.Who is Founder Of Google?",
    options: [
      "1. Bill Gates",
      "2. Larry Page",
      "3. Steve Jobs",
      "4. Elone Musk",
    ],
    answerIndex: 1,
  ),
  const QuizQuestions(
    question: "5.Who is Founder Of Android?",
    options: [
      "1. Bill Gates",
      "2. Andi Rubin",
      "3. Steve Jobs",
      "4. Elone Musk",
    ],
    answerIndex: 1,
  ),
];
