import 'package:flutter/material.dart';
import 'package:quiz_app/all_questions.dart';

bool questionScreen = true;
int questionIndex = 0;
int selectedIndex = -1;
int correctAns = 0;
int selectAns = -1;

MaterialStateProperty<Color> isColorChange(int buttonIndex) {
  if (selectedIndex != -1) {
    if (buttonIndex == allQuestions[questionIndex].answerIndex) {
      return const MaterialStatePropertyAll(Colors.green);
    } else if (buttonIndex == selectedIndex) {
      return const MaterialStatePropertyAll(Colors.red);
    } else {
      return const MaterialStatePropertyAll(Colors.white);
    }
  } else {
    return const MaterialStatePropertyAll(Colors.white);
  }
}

