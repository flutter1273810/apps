import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:quiz_app/colors.dart';
import 'package:quiz_app/all_questions.dart';

class MyQuizApp extends StatefulWidget {
  const MyQuizApp({super.key});

  @override
  State<MyQuizApp> createState() => MyQuizAppState();
}

class MyQuizAppState extends State<MyQuizApp> {
  Scaffold isQuestionScreen() {
    if (questionScreen == true) {
      return Scaffold(
        body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.center,
              tileMode: TileMode.clamp,
              stops: [0.5, 0.3],
              colors: [
                Color.fromARGB(255, 0, 255, 221),
                Colors.amber,
              ],
            ),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 40,
                ),
                SizedBox(
                  child: Text(
                    "Questions : ${questionIndex + 1}/${allQuestions.length}",
                    style: const TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                Container(
                  height: 150,
                  width: 380,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                    boxShadow: const [
                      BoxShadow(
                        color: Colors.black,
                        offset: Offset(8, 10),
                        blurRadius: 10,
                      ),
                    ],
                    border: Border.all(
                      color: Colors.black,
                      width: 3,
                    ),
                  ),
                  child: Center(
                    child: Text(
                      "${allQuestions[questionIndex].question}",
                      style: const TextStyle(
                        fontSize: 23,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 60,
                ),
                SizedBox(
                  height: 65,
                  width: 280,
                  child: ElevatedButton(
                    onPressed: () {
                      if (selectAns == -1) {
                        setState(() {
                          selectedIndex = 0;
                          if (selectAns != -1) {
                            return;
                          }
                          if (selectedIndex ==
                              allQuestions[questionIndex].answerIndex) {
                            correctAns++;
                          }
                          selectAns = 1;
                        });
                      }
                    },
                    style: ButtonStyle(backgroundColor: isColorChange(0)),
                    child: Text(
                      "${allQuestions[questionIndex].options[0]}",
                      style: const TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w500,
                          color: Colors.black),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                SizedBox(
                  height: 65,
                  width: 280,
                  child: ElevatedButton(
                    onPressed: () {
                      if (selectAns == -1) {
                        setState(() {
                          selectedIndex = 1;
                          if (selectAns != -1) {
                            return;
                          }
                          if (selectedIndex ==
                              allQuestions[questionIndex].answerIndex) {
                            correctAns++;
                          }
                          selectAns = 1;
                        });
                      }
                    },
                    style: ButtonStyle(backgroundColor: isColorChange(1)),
                    child: Text(
                      "${allQuestions[questionIndex].options[1]}",
                      style: const TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w500,
                          color: Colors.black),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                SizedBox(
                  height: 65,
                  width: 280,
                  child: ElevatedButton(
                    onPressed: () {
                      if (selectAns == -1) {
                        setState(() {
                          selectedIndex = 2;
                          if (selectAns != -1) {
                            return;
                          }
                          if (selectedIndex ==
                              allQuestions[questionIndex].answerIndex) {
                            correctAns++;
                          }
                          selectAns = 1;
                        });
                      }
                    },
                    style: ButtonStyle(backgroundColor: isColorChange(2)),
                    child: Text(
                      "${allQuestions[questionIndex].options[2]}",
                      style: const TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w500,
                          color: Colors.black),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                SizedBox(
                  height: 65,
                  width: 280,
                  child: ElevatedButton(
                    onPressed: () {
                      if (selectAns == -1) {
                        setState(() {
                          selectedIndex = 3;
                          if (selectAns != -1) {
                            return;
                          }
                          if (selectedIndex ==
                              allQuestions[questionIndex].answerIndex) {
                            correctAns++;
                          }
                          selectAns = 1;
                        });
                      }
                    },
                    style: ButtonStyle(backgroundColor: isColorChange(3)),
                    child: Text(
                      "${allQuestions[questionIndex].options[3]}",
                      style: const TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.w500,
                          color: Colors.black),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              if (selectedIndex == -1) {
                return;
              }
              questionIndex++;
              selectedIndex = -1;
              if (questionIndex == allQuestions.length) {
                questionScreen = false;
              }
              selectAns = -1;
            });
          },
          backgroundColor: Colors.black,
          child: const Text(
            "Next",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
            ),
          ),
        ),
      );
    } else {
      return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 30,
              ),
              SizedBox(
                height: 400,
                child: Image.asset("images/congratulation.webp"),
              ),
              const SizedBox(
                child: Text(
                  "Congratulation !!!",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                child: Text(
                  "Your Score : $correctAns/${allQuestions.length}",
                  style: const TextStyle(
                    fontSize: 27,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              SizedBox(
                height: 50,
                width: 150,
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      questionScreen = true;
                      questionIndex = 0;
                      selectedIndex = -1;
                      correctAns = 0;
                    });
                  },
                  style: const ButtonStyle(
                      backgroundColor:
                          MaterialStatePropertyAll(Colors.deepOrangeAccent)),
                  child: const Text(
                    "Reset",
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.w500,
                        color: Colors.black),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.red,
        title: Text(
          "Quiz App",
          style: GoogleFonts.quicksand(
            fontSize: 28,
            fontWeight: FontWeight.w700,
            color: Colors.black,
          ),
        ),
      ),
      body: isQuestionScreen(),
    );
  }
}
