class DataList {
  int? taskId;
  String title;
  String description;
  String date;
  int flag;

  DataList({
    this.taskId,
    required this.title,
    required this.description,
    required this.date,
    required this.flag,
  });

  Map<String, dynamic> mapEntry() {
    return {
      "taskId": taskId,
      "title": title,
      "description": description,
      "date": date,
      "flag": flag,
    };
  }
}

// List dataList = [
//   DataList(
//     title: "Lorem Ipsum is simply dummy industry. ",
//     description:
//         "Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ",
//     date: "10 July 2023",
//     flag: true,
//   ),
//   DataList(
//     title: "Lorem Ipsum is simply dummy industry. ",
//     description:
//         "Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ",
//     date: "10 July 2023",
//     flag: true,
//   ),
// ];
