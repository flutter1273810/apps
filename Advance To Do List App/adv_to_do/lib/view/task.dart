import 'package:adv_to_do/controller/sqf_lite.dart';
import 'package:adv_to_do/model/data_list.dart';
import 'package:adv_to_do/view/edit_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';

class Task extends StatelessWidget {
  final VoidCallback callback;
  const Task({super.key, required this.callback});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dbData.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
          child: Slidable(
            endActionPane: ActionPane(
              extentRatio: 0.15,
              motion: const ScrollMotion(),
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(
                      width: 10,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        EditSheet(
                          callback: callback,
                          index: index,
                          data: dbData.elementAt(index),
                        ),
                        GestureDetector(
                          onTap: () {
                            // myDataList.removeAt(index);
                            deleteData(dbData[index]["title"]);
                            getData(callback);
                            callback();
                          },
                          child: Container(
                            height: 31.92,
                            width: 31.92,
                            decoration: const BoxDecoration(
                              color: Color.fromRGBO(89, 57, 241, 1),
                              shape: BoxShape.circle,
                            ),
                            child: const Icon(
                              Icons.delete,
                              color: Colors.white,
                              size: 17,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            child: Container(
              height: 90,
              width: double.infinity,
              decoration: const BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.08),
                    blurRadius: 20,
                    offset: Offset(0, 4),
                  ),
                ],
                color: Color.fromRGBO(255, 255, 255, 1),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 52,
                          width: 52,
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color.fromRGBO(217, 217, 217, 1),
                          ),
                          child: Image.asset("images/todoLogo.png"),
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          dbData[index]["title"],
                          style: GoogleFonts.inter(
                            fontSize: 11,
                            fontWeight: FontWeight.w500,
                            color: const Color.fromRGBO(0, 0, 0, 1),
                          ),
                        ),
                        SizedBox(
                          width: 235,
                          height: 28,
                          child: Text(
                            dbData[index]["description"],
                            style: GoogleFonts.inter(
                              fontSize: 9,
                              fontWeight: FontWeight.w400,
                              color: const Color.fromRGBO(0, 0, 0, 1),
                            ),
                          ),
                        ),
                        Text(
                          dbData[index]["date"],
                          style: GoogleFonts.inter(
                            fontSize: 8,
                            fontWeight: FontWeight.w400,
                            color: const Color.fromRGBO(0, 0, 0, 1),
                          ),
                        ),
                      ],
                    ),
                    const Spacer(),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            if (dbData[index]["flag"] == 1) {
                              updateData(
                                DataList(
                                  taskId: dbData[index]["taskId"],
                                  title: dbData[index]["title"],
                                  description: dbData[index]["description"],
                                  date: dbData[index]["date"],
                                  flag: 0,
                                ),
                              );
                              callback();
                              getData(callback);
                            } else {
                              updateData(
                                DataList(
                                  taskId: dbData[index]["taskId"],
                                  title: dbData[index]["title"],
                                  description: dbData[index]["description"],
                                  date: dbData[index]["date"],
                                  flag: 1,
                                ),
                              );
                              callback();
                              getData(callback);
                            }
                            callback();
                          },
                          // onTap: () {
                          //   if (dbData[index].flag == 1) {
                          //     dbData[index].flag = 0;
                          //   } else {
                          //     dbData[index].flag = 1;
                          //   }
                          //   callback();
                          // },
                          child: Container(
                            height: 15,
                            width: 15,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                color: const Color.fromRGBO(0, 0, 0, 0.5),
                              ),
                              color: (dbData[index]["flag"] == 1)
                                  ? const Color.fromRGBO(255, 255, 255, 1)
                                  : const Color.fromRGBO(4, 189, 0, 1),
                            ),
                            child: Image.asset("images/right.png"),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

// class Task extends StatefulWidget {
//   final VoidCallback callback;
//   const Task({super.key, required this.callback});

//   @override
//   State<Task> createState() => _TaskState(callback: callback);
// }

// class _TaskState extends State<Task> {
//   final VoidCallback callback;
//   _TaskState({required this.callback});
//   void refresh() {
//     setState(() {});
//   }

//   @override
//   Widget build(BuildContext context) {
//     return ListView.builder(
//       itemCount: dataList.length,
//       itemBuilder: (context, index) {
//         return Padding(
//           padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
//           child: Slidable(
//             endActionPane: ActionPane(
//               extentRatio: 0.15,
//               motion: const ScrollMotion(),
//               children: [
//                 Row(
//                   mainAxisSize: MainAxisSize.min,
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     const SizedBox(
//                       width: 10,
//                     ),
//                     Column(
//                       mainAxisAlignment: MainAxisAlignment.spaceAround,
//                       children: [
//                         GestureDetector(
//                           child: Container(
//                             height: 31.92,
//                             width: 31.92,
//                             decoration: const BoxDecoration(
//                               color: Color.fromRGBO(89, 57, 241, 1),
//                               shape: BoxShape.circle,
//                             ),
//                             child: const Icon(
//                               Icons.edit,
//                               color: Colors.white,
//                               size: 17,
//                             ),
//                           ),
//                         ),
//                         GestureDetector(
//                           onTap: () {
//                             setState(() {
//                               dataList.removeAt(index);
//                             });
//                           },
//                           child: Container(
//                             height: 31.92,
//                             width: 31.92,
//                             decoration: const BoxDecoration(
//                               color: Color.fromRGBO(89, 57, 241, 1),
//                               shape: BoxShape.circle,
//                             ),
//                             child: const Icon(
//                               Icons.delete,
//                               color: Colors.white,
//                               size: 17,
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ],
//                 ),
//               ],
//             ),
//             child: Container(
//               height: 90,
//               width: double.infinity,
//               decoration: const BoxDecoration(
//                 boxShadow: [
//                   BoxShadow(
//                     color: Color.fromRGBO(0, 0, 0, 0.08),
//                     blurRadius: 20,
//                     offset: Offset(0, 4),
//                   ),
//                 ],
//                 color: Color.fromRGBO(255, 255, 255, 1),
//               ),
//               child: Padding(
//                 padding: const EdgeInsets.all(10.0),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     Column(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: [
//                         Container(
//                           height: 52,
//                           width: 52,
//                           alignment: Alignment.center,
//                           decoration: const BoxDecoration(
//                             shape: BoxShape.circle,
//                             color: Color.fromRGBO(217, 217, 217, 1),
//                           ),
//                           child: Image.asset("images/todoLogo.png"),
//                         ),
//                       ],
//                     ),
//                     Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       mainAxisAlignment: MainAxisAlignment.spaceAround,
//                       children: [
//                         Text(
//                           dataList[index].title,
//                           style: GoogleFonts.inter(
//                             fontSize: 11,
//                             fontWeight: FontWeight.w500,
//                             color: const Color.fromRGBO(0, 0, 0, 1),
//                           ),
//                         ),
//                         SizedBox(
//                           width: 235,
//                           height: 28,
//                           child: Text(
//                             dataList[index].description,
//                             style: GoogleFonts.inter(
//                               fontSize: 9,
//                               fontWeight: FontWeight.w400,
//                               color: const Color.fromRGBO(0, 0, 0, 1),
//                             ),
//                           ),
//                         ),
//                         Text(
//                           dataList[index].date,
//                           style: GoogleFonts.inter(
//                             fontSize: 8,
//                             fontWeight: FontWeight.w400,
//                             color: const Color.fromRGBO(0, 0, 0, 1),
//                           ),
//                         ),
//                       ],
//                     ),
//                     Column(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       children: [
//                         GestureDetector(
//                           onTap: () {
//                             setState(() {
//                               if (dataList[index].flag == true) {
//                                 dataList[index].flag = false;
//                               } else {
//                                 dataList[index].flag = true;
//                               }
//                             });
//                           },
//                           child: Container(
//                             height: 15,
//                             width: 15,
//                             decoration: BoxDecoration(
//                               shape: BoxShape.circle,
//                               border: Border.all(
//                                 color: const Color.fromRGBO(0, 0, 0, 0.5),
//                               ),
//                               color: (dataList[index].flag)
//                                   ? const Color.fromRGBO(255, 255, 255, 1)
//                                   : const Color.fromRGBO(4, 189, 0, 1),
//                             ),
//                             child: Image.asset("images/right.png"),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//           ),
//         );
//       },
//     );
//   }
// }
