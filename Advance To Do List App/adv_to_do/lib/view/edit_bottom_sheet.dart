import 'package:adv_to_do/controller/sqf_lite.dart';
import 'package:adv_to_do/model/data_list.dart';
// import 'package:adv_to_do/model/data_list.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class EditSheet extends StatelessWidget {
  final VoidCallback callback;
  final int index;
  final Map data;

  const EditSheet(
      {super.key,
      required this.callback,
      required this.index,
      required this.data});

  @override
  Widget build(BuildContext context) {
    TextEditingController editTitle = TextEditingController();
    TextEditingController editDescription = TextEditingController();
    TextEditingController editDate = TextEditingController();

    editTitle.text = dbData[index]["title"];
    editDescription.text = dbData[index]["description"];
    editDate.text = dbData[index]["date"];

    return GestureDetector(
      onTap: () {
        showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          builder: (context) {
            return Padding(
              padding: MediaQuery.of(context).viewInsets,
              child: SizedBox(
                width: double.infinity,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        "Edit To-Do ",
                        style: GoogleFonts.quicksand(
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                          color: const Color.fromRGBO(0, 0, 0, 1),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(
                              height: 20,
                            ),
                            Text(
                              "Title",
                              style: GoogleFonts.quicksand(
                                fontSize: 11,
                                fontWeight: FontWeight.w400,
                                color: const Color.fromRGBO(89, 57, 241, 1),
                              ),
                            ),
                            SizedBox(
                              height: 40,
                              width: double.infinity,
                              child: TextField(
                                controller: editTitle,
                                cursorColor:
                                    const Color.fromRGBO(89, 57, 241, 1),
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  enabledBorder: const OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color.fromRGBO(89, 57, 241, 1),
                                    ),
                                  ),
                                  focusedBorder: const OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color.fromRGBO(89, 57, 241, 1),
                                      width: 1.5,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Text(
                              "Description",
                              style: GoogleFonts.quicksand(
                                fontSize: 11,
                                fontWeight: FontWeight.w400,
                                color: const Color.fromRGBO(89, 57, 241, 1),
                              ),
                            ),
                            SizedBox(
                              height: 72,
                              width: double.infinity,
                              child: TextField(
                                controller: editDescription,
                                cursorColor:
                                    const Color.fromRGBO(89, 57, 241, 1),
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  enabledBorder: const OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color.fromRGBO(89, 57, 241, 1),
                                    ),
                                  ),
                                  focusedBorder: const OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color.fromRGBO(89, 57, 241, 1),
                                      width: 1.5,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Text(
                              "Date",
                              style: GoogleFonts.quicksand(
                                fontSize: 11,
                                fontWeight: FontWeight.w400,
                                color: const Color.fromRGBO(89, 57, 241, 1),
                              ),
                            ),
                            SizedBox(
                              height: 40,
                              width: double.infinity,
                              child: TextField(
                                controller: editDate,
                                readOnly: true,
                                cursorColor:
                                    const Color.fromRGBO(89, 57, 241, 1),
                                decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    onPressed: () async {
                                      DateTime? pickDate = await showDatePicker(
                                        context: context,
                                        initialDate: DateTime.now(),
                                        firstDate: DateTime(2022),
                                        lastDate: DateTime(2025),
                                      );
                                      String dateFormat =
                                          DateFormat.yMMMMd().format(pickDate!);

                                      editDate.text = dateFormat;
                                    },
                                    icon: const Icon(
                                      Icons.calendar_month_outlined,
                                      weight: 1,
                                      size: 20,
                                    ),
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  enabledBorder: const OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color.fromRGBO(89, 57, 241, 1),
                                    ),
                                  ),
                                  focusedBorder: const OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color.fromRGBO(89, 57, 241, 1),
                                      width: 1.5,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 50,
                        width: 300,
                        child: ElevatedButton(
                          onPressed: () {
                            if (editTitle.text.isEmpty ||
                                editDescription.text.isEmpty ||
                                editDate.text.isEmpty) {
                              return;
                            }
                            Navigator.pop(context);

                            DataList obj = DataList(
                              taskId: dbData[index]["taskId"],
                              title: editTitle.text.trim(),
                              description: editDescription.text.trim(),
                              date: editDate.text.trim(),
                              flag: 1,
                            );
                            updateData(obj);
                            getData(callback);
                            callback();
                          },
                          style: ButtonStyle(
                            backgroundColor: const WidgetStatePropertyAll(
                              Color.fromRGBO(89, 57, 241, 1),
                            ),
                            shape: WidgetStatePropertyAll(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ),
                          child: Text(
                            "Submit",
                            style: GoogleFonts.inter(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: const Color.fromRGBO(255, 255, 255, 1),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        );
        callback();
      },
      child: Container(
        height: 31.92,
        width: 31.92,
        decoration: const BoxDecoration(
          color: Color.fromRGBO(89, 57, 241, 1),
          shape: BoxShape.circle,
        ),
        child: const Icon(
          Icons.edit,
          color: Colors.white,
          size: 17,
        ),
      ),
    );
  }
}


// class EditSheet extends StatelessWidget {
//   final VoidCallback callback;

//   const EditSheet({super.key, required this.callback});

//   @override
//   Widget build(BuildContext context) {
//     TextEditingController titleController = TextEditingController();
//     TextEditingController descriptionController = TextEditingController();
//     TextEditingController dateController = TextEditingController();
//     return FloatingActionButton(
//       shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
//       onPressed: () {
//         callback();
//         showModalBottomSheet(
//           context: context,
//           isScrollControlled: true,
//           builder: (context) {
//             return Padding(
//               padding: MediaQuery.of(context).viewInsets,
//               child: SizedBox(
//                 width: double.infinity,
//                 child: Padding(
//                   padding:
//                       const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
//                   child: Column(
//                     mainAxisSize: MainAxisSize.min,
//                     children: [
//                       Text(
//                         "Edit To-Do ",
//                         style: GoogleFonts.quicksand(
//                           fontSize: 22,
//                           fontWeight: FontWeight.w600,
//                           color: const Color.fromRGBO(0, 0, 0, 1),
//                         ),
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.all(10.0),
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             const SizedBox(
//                               height: 20,
//                             ),
//                             Text(
//                               "Title",
//                               style: GoogleFonts.quicksand(
//                                 fontSize: 11,
//                                 fontWeight: FontWeight.w400,
//                                 color: const Color.fromRGBO(89, 57, 241, 1),
//                               ),
//                             ),
//                             SizedBox(
//                               height: 40,
//                               width: double.infinity,
//                               child: TextField(
//                                 controller: titleController,
//                                 cursorColor:
//                                     const Color.fromRGBO(89, 57, 241, 1),
//                                 decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                     borderRadius: BorderRadius.circular(5),
//                                   ),
//                                   enabledBorder: const OutlineInputBorder(
//                                     borderSide: BorderSide(
//                                       color: Color.fromRGBO(89, 57, 241, 1),
//                                     ),
//                                   ),
//                                   focusedBorder: const OutlineInputBorder(
//                                     borderSide: BorderSide(
//                                       color: Color.fromRGBO(89, 57, 241, 1),
//                                       width: 1.5,
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                             ),
//                             const SizedBox(
//                               height: 20,
//                             ),
//                             Text(
//                               "Description",
//                               style: GoogleFonts.quicksand(
//                                 fontSize: 11,
//                                 fontWeight: FontWeight.w400,
//                                 color: const Color.fromRGBO(89, 57, 241, 1),
//                               ),
//                             ),
//                             SizedBox(
//                               height: 72,
//                               width: double.infinity,
//                               child: TextField(
//                                 controller: descriptionController,
//                                 cursorColor:
//                                     const Color.fromRGBO(89, 57, 241, 1),
//                                 decoration: InputDecoration(
//                                   border: OutlineInputBorder(
//                                     borderRadius: BorderRadius.circular(5),
//                                   ),
//                                   enabledBorder: const OutlineInputBorder(
//                                     borderSide: BorderSide(
//                                       color: Color.fromRGBO(89, 57, 241, 1),
//                                     ),
//                                   ),
//                                   focusedBorder: const OutlineInputBorder(
//                                     borderSide: BorderSide(
//                                       color: Color.fromRGBO(89, 57, 241, 1),
//                                       width: 1.5,
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                             ),
//                             const SizedBox(
//                               height: 20,
//                             ),
//                             Text(
//                               "Date",
//                               style: GoogleFonts.quicksand(
//                                 fontSize: 11,
//                                 fontWeight: FontWeight.w400,
//                                 color: const Color.fromRGBO(89, 57, 241, 1),
//                               ),
//                             ),
//                             SizedBox(
//                               height: 40,
//                               width: double.infinity,
//                               child: TextField(
//                                 controller: dateController,
//                                 readOnly: true,
//                                 cursorColor:
//                                     const Color.fromRGBO(89, 57, 241, 1),
//                                 decoration: InputDecoration(
//                                   suffixIcon: IconButton(
//                                     onPressed: () async {
//                                       // showDatePicker(
//                                       //   context: context,
//                                       //   initialDate: DateTime.now(),
//                                       //   firstDate: DateTime(2022),
//                                       //   lastDate: DateTime(2026),
//                                       // );
//                                       // DateFormat.yMMMMd();
//                                       DateTime? pickDate = await showDatePicker(
//                                         context: context,
//                                         initialDate: DateTime.now(),
//                                         firstDate: DateTime(2022),
//                                         lastDate: DateTime(2025),
//                                       );
//                                       String dateFormat =
//                                           DateFormat.yMMMMd().format(pickDate!);

//                                       dateController.text = dateFormat;
//                                     },
//                                     icon: const Icon(
//                                       Icons.calendar_month_outlined,
//                                       weight: 1,
//                                       size: 20,
//                                     ),
//                                   ),
//                                   border: OutlineInputBorder(
//                                     borderRadius: BorderRadius.circular(5),
//                                   ),
//                                   enabledBorder: const OutlineInputBorder(
//                                     borderSide: BorderSide(
//                                       color: Color.fromRGBO(89, 57, 241, 1),
//                                     ),
//                                   ),
//                                   focusedBorder: const OutlineInputBorder(
//                                     borderSide: BorderSide(
//                                       color: Color.fromRGBO(89, 57, 241, 1),
//                                       width: 1.5,
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                             ),
//                             const SizedBox(
//                               height: 20,
//                             ),
//                           ],
//                         ),
//                       ),
//                       SizedBox(
//                         height: 50,
//                         width: 300,
//                         child: ElevatedButton(
//                           onPressed: () {
//                             if (titleController.text.isEmpty ||
//                                 descriptionController.text.isEmpty ||
//                                 dateController.text.isEmpty) {
//                               return;
//                             }
//                             Navigator.pop(context);

//                             dataList.add(
//                               DataList(
//                                 title: titleController.text.trim(),
//                                 description: descriptionController.text.trim(),
//                                 date: dateController.text.trim(),
//                                 flag: true,
//                               ),
//                             );

//                             titleController.clear();
//                             descriptionController.clear();
//                             dateController.clear();
//                             callback();
//                           },
//                           style: ButtonStyle(
//                             backgroundColor: const MaterialStatePropertyAll(
//                               Color.fromRGBO(89, 57, 241, 1),
//                             ),
//                             shape: MaterialStatePropertyAll(
//                               RoundedRectangleBorder(
//                                 borderRadius: BorderRadius.circular(10),
//                               ),
//                             ),
//                           ),
//                           child: Text(
//                             "Submit",
//                             style: GoogleFonts.inter(
//                               fontSize: 20,
//                               fontWeight: FontWeight.w700,
//                               color: const Color.fromRGBO(255, 255, 255, 1),
//                             ),
//                           ),
//                         ),
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//             );
//           },
//         );
//       },
//       backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
//       child: const Icon(
//         Icons.add,
//         size: 40,
//         color: Color.fromRGBO(255, 255, 255, 1),
//       ),
//     );
//   }
// }





