import 'package:adv_to_do/model/data_list.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

List dbData = [];
dynamic database;

void createDatabase(VoidCallback callback) async {
  database = await openDatabase(
    join(await getDatabasesPath(), "Todoappdb.db"),
    version: 1,
    onCreate: (db, version) async {
      await db.execute('''
          CREATE TABLE todoData(
            taskId INTEGER PRIMARY KEY AUTOINCREMENT,
            title TEXT,
            description TEXT,
            date TEXT,
            flag INT
          )
        ''');
    },
  );
  print(await getDatabasesPath());
  getData(callback);
}

void insetData(DataList obj) async {
  Database localDb = await database;

  await localDb.insert(
    "todoData",
    obj.mapEntry(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
  print("Insert Succesfully");
}

void getData(VoidCallback callback) async {
  Database localDb = await database;

  dbData = await localDb.query("todoData");
  print(dbData);
  callback();
}

void updateData(DataList obj) async {
  Database localDb = await database;

  await localDb.update(
    "todoData",
    obj.mapEntry(),
    where: "taskId=?",
    whereArgs: [obj.taskId],
  );
  print("Update Data Succesfully");
}

void deleteData(String str) async {
  Database localDb = await database;

  await localDb.delete(
    "todoData",
    where: "title=?",
    whereArgs: [str],
  );
}

// List myDataList = [];

// getData(VoidCallback callback) async {
//   final localDB = await database;
//   List<Map<String, dynamic>> myDataListloc = await localDB.query("todoData");

//   List datas = List.generate(myDataListloc.length, (index) {
//     return myDataListloc[index];
//   });
//   myDataList = datas;

//   print(myDataListloc);
//   callback();
// }
